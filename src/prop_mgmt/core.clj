(ns prop-mgmt.core
  (:require [org.clojure.java.jdbc :as sql]))

(def db
  {:classname   "org.sqlite.JDBC"
   :subprotocol "sqlite"
   :subname     "propmgmt.sqlite"
   })

(sql/with-connection db
  (sql/do-commands "pragma foreign_keys=ON")
  (sql/...)
  (sql/with-query-results res
    ["select item_name, amount, date, vendor, from_date, to_date from history, leases where history.prop_name = '1313 Mockingbird Ln' and leases.prop_name = '1313 Mockingbird Ln' and leases.from_date <= date('now') and leases.to_date >= date('now') and history.date >= leases.from_date"]
    (doall res)))

(defn hello [x]
  (x))
